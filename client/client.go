// This is the main client for calling the grep command.
package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	pb "mp1/protos"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

var vm_machine_ips = [10]string{"fa22-cs425-0301:50052", "fa22-cs425-0302:50052", "fa22-cs425-0303:50052", "fa22-cs425-0304:50052", "fa22-cs425-0305:50052",
	"fa22-cs425-0306:50052", "fa22-cs425-0307:50052", "fa22-cs425-0308:50052", "fa22-cs425-0309:50052", "fa22-cs425-0310:50052"}

var local_machine_ips = [1]string{"localhost:50051"}

var ip_addrs = vm_machine_ips

func main() {
	flag.Parse()
	args := os.Args[1:]
	start := time.Now()
	for _, addr := range vm_machine_ips {
		conn, err := grpc.Dial(addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
		if err != nil {
			log.Printf("did not connect: %v", err)
		} else {
			defer conn.Close()
			c := pb.NewDistributedGrepClient(conn)

			ctx, cancel := context.WithTimeout(context.Background(), time.Second*20)
			defer cancel()
			searchOptions := args[0]
			searchString := args[1]
			searchFile := args[2]
			r, err := c.CallGrep(ctx, &pb.ClientRequest{QueryString: searchString, Args: searchOptions, QueryFile: searchFile})
			if err != nil {
				log.Printf("could not grep!: %v", err)
			} else {
				duration := time.Since(start)
				for _, item := range r.GetResult() {
					fmt.Printf("%s", item)
				}
				fmt.Printf("\nTotal Matches : %d\n", r.GetTotal())
				fmt.Printf("Query Time : %v\n", duration)
			}
		}
	}
}
