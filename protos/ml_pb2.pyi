from google.protobuf.internal import containers as _containers
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Iterable as _Iterable, Optional as _Optional

DESCRIPTOR: _descriptor.FileDescriptor

class JobRequest(_message.Message):
    __slots__ = ["batchSize", "fileNames", "modelName"]
    BATCHSIZE_FIELD_NUMBER: _ClassVar[int]
    FILENAMES_FIELD_NUMBER: _ClassVar[int]
    MODELNAME_FIELD_NUMBER: _ClassVar[int]
    batchSize: int
    fileNames: _containers.RepeatedScalarFieldContainer[str]
    modelName: str
    def __init__(self, modelName: _Optional[str] = ..., batchSize: _Optional[int] = ..., fileNames: _Optional[_Iterable[str]] = ...) -> None: ...

class JobResponse(_message.Message):
    __slots__ = []
    def __init__(self) -> None: ...

class KillRequest(_message.Message):
    __slots__ = []
    def __init__(self) -> None: ...

class KillResponse(_message.Message):
    __slots__ = ["hostName"]
    HOSTNAME_FIELD_NUMBER: _ClassVar[int]
    hostName: str
    def __init__(self, hostName: _Optional[str] = ...) -> None: ...

class ScheduleRequest(_message.Message):
    __slots__ = ["batchNum", "fileNames", "jobId", "modelName"]
    BATCHNUM_FIELD_NUMBER: _ClassVar[int]
    FILENAMES_FIELD_NUMBER: _ClassVar[int]
    JOBID_FIELD_NUMBER: _ClassVar[int]
    MODELNAME_FIELD_NUMBER: _ClassVar[int]
    batchNum: int
    fileNames: _containers.RepeatedScalarFieldContainer[str]
    jobId: int
    modelName: str
    def __init__(self, modelName: _Optional[str] = ..., fileNames: _Optional[_Iterable[str]] = ..., batchNum: _Optional[int] = ..., jobId: _Optional[int] = ...) -> None: ...

class ScheduleResponse(_message.Message):
    __slots__ = []
    def __init__(self) -> None: ...

class SendAckMsg(_message.Message):
    __slots__ = []
    def __init__(self) -> None: ...

class SendAckRequest(_message.Message):
    __slots__ = ["vmAddr"]
    VMADDR_FIELD_NUMBER: _ClassVar[int]
    vmAddr: str
    def __init__(self, vmAddr: _Optional[str] = ...) -> None: ...

class SendAckResponse(_message.Message):
    __slots__ = []
    def __init__(self) -> None: ...
