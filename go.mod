module mp1

go 1.19

require (
	golang.org/x/exp v0.0.0-20220921164117-439092de6870
	google.golang.org/grpc v1.51.0
	google.golang.org/protobuf v1.28.1
)

require (
	github.com/chrusty/go-tableprinter v0.0.0-20190528113659-0de6c8f09400 // indirect
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/dustin/go-humanize v1.0.0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/kataras/tablewriter v0.0.0-20180708051242-e063d29b7c23 // indirect
	github.com/lensesio/tableprinter v0.0.0-20201125135848-89e81fc956e7 // indirect
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/olekukonko/tablewriter v0.0.1 // indirect
	github.com/rivo/uniseg v0.4.2 // indirect
	golang.org/x/net v0.2.0 // indirect
	golang.org/x/sys v0.2.0 // indirect
	golang.org/x/text v0.4.0 // indirect
	google.golang.org/genproto v0.0.0-20221202195650-67e5cbc046fd // indirect
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.2.0 // indirect
)
