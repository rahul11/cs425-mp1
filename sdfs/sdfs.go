package sdfs

import (
	"bufio"
	"context"
	"errors"
	"fmt"
	"hash/fnv"
	"io"
	"io/ioutil"
	"log"
	"math"
	"math/rand"
	"mp1/peer"
	pb "mp1/protos"
	"net"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type SDFSServer struct {
	pb.UnimplementedSDFSInterfaceServer
}

// This function is used to get an inverse table mapping of the Membership List
// It will have a file->list of hosts with the file mapping for ease of computation
func getInverseTable(memberList []peer.Member) map[string](map[string]int) {
	inverseTable := make(map[string](map[string]int))
	for _, member := range memberList {
		for k, _ := range member.FileInfo {
			if inverseTable[k] == nil {
				inverseTable[k] = make(map[string]int)
			}
			inverseTable[k][member.HostName] = member.FileInfo[k]
		}
	}
	return inverseTable
}

// This is the main Delete server-side RPC Call - This is responsible for deleting files present at /replica location on the data node
// This also updates the membership list and removes the file from the list.
func (s *SDFSServer) Delete(ctx context.Context, in *pb.DeleteRequest) (*pb.DeleteResponse, error) {
	fmt.Sprintf("Delete request received %v %v \n", in.SdfsFileName, in.Primary)
	sdfsFileName := in.GetSdfsFileName()
	primary := in.GetPrimary()
	responseChan := make(chan bool)
	replicaLocation := "/replicas/" + sdfsFileName
	var resp pb.DeleteResponse
	var neighbours []string
	noOfAcks := 0
	if _, err := os.Stat(replicaLocation); errors.Is(err, os.ErrNotExist) {
		fmt.Sprintf("Error file doesnt exist during delete\n")
		resp.Ack = false
		return &resp, err
	} else {
		err := os.RemoveAll(replicaLocation + "/")
		if err == nil {
			fmt.Printf("folder deleted success on %s\n", peer.MyHostName)
			//update mem list
			peer.Lock.Lock()
			for _, member := range peer.MemberList {
				if member.HostName == peer.MyHostName {
					delete(member.FileInfo, sdfsFileName)
					break
				}
			}
			peer.Lock.Unlock()
		} else {
			fmt.Printf("Delete failed on folder %s %v\n", peer.MyHostName, err)
		}
		if primary {
			// Find and remove self
			peer.Lock.RLock()
			for _, member := range peer.MemberList {
				if _, ok := member.FileInfo[sdfsFileName]; ok {
					if member.HostName != peer.MyHostName {
						neighbours = append(neighbours, member.HostName)
					}
				}
			}
			peer.Lock.RUnlock()
			fmt.Printf("Deleting at neighbours %v\n", neighbours)
			for _, neighbour := range neighbours {
				go del(neighbour, sdfsFileName, responseChan, false)
				select {
				case res := <-responseChan:
					if res {
						noOfAcks = noOfAcks + 1
						fmt.Printf("noofacks %d\n", noOfAcks)
					}
					if noOfAcks == peer.NUM_REPLICAS-1 {
						fmt.Printf("noofacks %d\n", noOfAcks)
						resp.Ack = true
						return &resp, nil
					}
				case <-time.After(10 * time.Second):
					fmt.Println("timeout after waiting for replicas to delete\n")
					resp.Ack = false
					return &resp, nil
				}
			}
		} else {
			fmt.Printf("Deleting at secondary\n")
			resp.Ack = true
			return &resp, err
		}
	}
	resp.Ack = false
	return &resp, nil
}

// This is a helper function for del that calls the delete client to delete in all replicas by the primary
func del(hostname string, sdfsFileName string, responseChan chan bool, primary bool) {
	response := delClient(hostname, sdfsFileName, primary)
	responseChan <- response
}

// This is the cron thread that runs every 10s to fix for the following failures
// 1. Incase a node has left the membership list, the primary will re-replicate to another randomly picked node.
// 2. Incase a node does not have the latest version of the file, the primary will re-replicate the latest version of the file.
// 3. Incase a node does not have the latest
func FixReplicas() {
	for {
		time.Sleep(time.Second * 10)
		peer.Lock.RLock()
		tempList := make([]peer.Member, len(peer.MemberList))
		copy(tempList, peer.MemberList)
		peer.Lock.RUnlock()
		inverseTable := getInverseTable(tempList)
		for file_name, file := range inverseTable {
			highestVersionIp := ""
			for ip, version := range file {
				if highestVersionIp == "" || file[highestVersionIp] < version {
					highestVersionIp = ip
				}
			}
			for ip, version := range file {
				if file[highestVersionIp] != version && highestVersionIp == peer.MyHostName {
					node := fmt.Sprintf("%s:%d", ip, peer.SDFS_TCP_PORT)
					fmt.Printf("I am primary fixing file %s with version number %d at %s\n", file_name, file[highestVersionIp], node)
					if _, err := os.Stat("/tmp/" + file_name); errors.Is(err, os.ErrNotExist) {
						fmt.Sprintf("Error file doesnt exist during delete\n")
						response := streamAndWrite(node, fmt.Sprintf("/replicas/%s/%s", file_name, file_name), file_name, false, fmt.Sprintf("%d", file[highestVersionIp]), false)
						if response.Ack {
							fmt.Printf("Replicate %s to %s successful\n", file, node)
						} else {
							fmt.Printf("Replicate %s to %s unsuccessful\n", file, node)
						}
					}
				}
			}
		}
		if len(tempList) >= peer.NUM_REPLICAS {
			for file, hostnames := range inverseTable {
				if len(hostnames) < peer.NUM_REPLICAS {
					primary := findPrimaryFromMap(hostnames)
					if primary == peer.MyHostName {
						rand.Seed(time.Now().Unix()) // initialize global pseudo random generator
						var node_ip string
						for true {
							node_ip = tempList[rand.Intn(len(tempList))].HostName
							if _, ok := inverseTable[file][node_ip]; !ok {
								break
							} else {
								fmt.Printf("%s in replica list,retrying\n", node_ip)
							}
						}
						node := fmt.Sprintf("%s:%d", node_ip, peer.SDFS_TCP_PORT)
						dir := "/replicas/" + file
						command := fmt.Sprintf("ls -tr1 %s", dir)
						files_str, err := exec.Command("/bin/sh", "-c", command).CombinedOutput()
						if err != nil {
							log.Fatal(err)
						}

						files := strings.Split(strings.TrimSpace(string(files_str)), "\n")

						ack := 0
						noOfFiles := len(files)
						var version string
						var filename string
						// versionRepair := true
						var localFileName string
						fmt.Printf("no of files %d %v\n", noOfFiles, files)
						for _, file1 := range files {
							names := strings.Split(file1, "@")
							if len(names) == 2 {
								version = names[1]
								filename = names[0]
								localFileName = "/replicas/" + filename + "/" + filename + "@" + version
								fmt.Printf("Trying to replicate versions %s %s to %s\n", localFileName, version, node)
								response := streamAndWrite(node, localFileName, filename, false, version, true)
								if response.Ack {
									ack += 1
								}
							} else if len(names) == 1 {
								version = fmt.Sprintf("%d", hostnames[peer.MyHostName])
								filename = names[0]
								localFileName = "/replicas/" + filename + "/" + filename

								if _, err := os.Stat("/tmp/" + filename); errors.Is(err, os.ErrNotExist) {
									fmt.Printf("Trying to replicate latest %s %s to %s \n", localFileName, version, node)
									response := streamAndWrite(node, localFileName, filename, false, version, false)
									if response.Ack {
										fmt.Printf("Replicate %s to %s successful\n", file, node)
									} else {
										fmt.Printf("Replicate %s to %s unsuccessful\n", file, node)
									}
									if response.Ack {
										ack += 1
									}
								}
							}
						}
						if ack == noOfFiles {
							fmt.Printf("Replicate %s to %s successful\n", file, node)
						} else {
							fmt.Printf("Replicate %s to %s unsuccessful\n", file, node)
						}
					}
				}
			}
		}
	}
}

// The function to hash a file name to map it to a node in the clusters.
func hash(s string) uint32 {
	h := fnv.New32a()
	h.Write([]byte(s))
	return h.Sum32()
}

// This function is used to update the membership list with the most recent version of the file
func updateMemberList(sdfsFileName string) int {
	peer.Lock.Lock()
	defer peer.Lock.Unlock()

	for i, member := range peer.MemberList {
		if member.HostName == peer.MyHostName {
			peer.MemberList[i].FileInfo[sdfsFileName] += 1
			return peer.MemberList[i].FileInfo[sdfsFileName]
		}
	}
	//never going to reach here
	return -1
}

// The function is used to get the latest version of the file in the sdfs cluster.
func getVersion(sdfsFileName string) int {
	peer.Lock.RLock()
	defer peer.Lock.RUnlock()
	for _, member := range peer.MemberList {
		if member.HostName == peer.MyHostName {
			if _, ok := member.FileInfo[sdfsFileName]; ok {
				return member.FileInfo[sdfsFileName]
			}
		}
	}
	return -1
}

// This is the main server side commit RPC call that is used to commit a file that was previously put
// by the PUT client. The commit transfers a file from /tmp to /replicas and updates the membership list.
func (s *SDFSServer) Commit(ctx context.Context, in *pb.CommitRequest) (*pb.CommitResponse, error) {
	var resp pb.CommitResponse
	sdfsFileName := in.GetSdfsFileName()
	inBoundVersionNumber := in.GetVersion()
	var fileVersion int
	fmt.Printf("Commit Request Received %s \n", sdfsFileName)

	localVersionNumber := fmt.Sprintf("%d", getVersion(sdfsFileName))

	dir := "/tmp/"
	command := fmt.Sprintf("ls -tr1 %s", dir)
	files, err := exec.Command("/bin/sh", "-c", command).CombinedOutput()
	fmt.Printf("\n\nCOMMIT CHECK %s\n\n", files)
	if err != nil {
		log.Fatal(err)
	}
	//Check if tmp exists
	tmpLocation := "/tmp/" + sdfsFileName
	if _, err := os.Stat(tmpLocation); errors.Is(err, os.ErrNotExist) {
		fmt.Printf("File %v does not exist. Not committing", sdfsFileName)
	} else if err == nil {
		fmt.Printf("File exists. Committing the file %v\n", sdfsFileName)
		if localVersionNumber == inBoundVersionNumber {
			fmt.Printf("File %s:%d already synced with put repair, deleting temp\n", sdfsFileName, localVersionNumber)
			err := os.Remove(tmpLocation)
			if err != nil {
				fmt.Printf("Delete error %v", err)
				resp.Ack = false
			}
			resp.Ack = true
			return &resp, nil
		}
		newLocation := "/replicas/" + sdfsFileName
		if _, err := os.Stat(newLocation); errors.Is(err, os.ErrNotExist) {
			command := fmt.Sprintf("%s %s", peer.MKDIR_COMMAND_NAME, newLocation)
			cmd := exec.Command("/bin/sh", "-c", command)
			fmt.Printf(cmd.String() + "\n")
			_, err := cmd.CombinedOutput()
			if err != nil {
				fmt.Printf("folder create error %v", err)
				resp.Ack = false
			}
		}
		newLocation = newLocation + "/" + sdfsFileName
		if _, err := os.Stat(newLocation); err == nil {
			command := fmt.Sprintf("%s %s %s@%d", peer.MV_COMMAND_NAME, newLocation, newLocation, getVersion(sdfsFileName))
			cmd := exec.Command("/bin/sh", "-c", command)
			fmt.Printf(cmd.String() + "\n")
			_, err := cmd.CombinedOutput()
			if err != nil {
				fmt.Printf("move error %v", err)
				resp.Ack = false
				return &resp, nil
			}
			dir := "/replicas/" + sdfsFileName
			command = fmt.Sprintf("ls -tr1 %s", dir)
			files, err := exec.Command("/bin/sh", "-c", command).CombinedOutput()
			if err != nil {
				log.Fatal(err)
			}

			files_list := strings.Split(strings.TrimSpace(string(files)), "\n")
			if (len(files_list)) > 5 {
				todelete := files_list[0]

				fmt.Printf("Deleting extra versions %s\n", todelete)
				command := fmt.Sprintf("rm /replicas/%s/%s", sdfsFileName, todelete)
				cmd := exec.Command("/bin/sh", "-c", command)
				_, err := cmd.CombinedOutput()
				if err != nil {
					fmt.Printf("delete error %v %v\n", err, todelete)
					resp.Ack = false
					return &resp, nil
				}
			}
		}
		//Updating version is done here now! - Moved to Commit Request
		fileVersion = updateMemberList(sdfsFileName)
		fmt.Printf("New file version after commit %v %v", sdfsFileName, fileVersion)

		fmt.Printf("Committing %s to %s\n", tmpLocation, newLocation)
		command := fmt.Sprintf("%s %s %s", peer.MV_COMMAND_NAME, tmpLocation, newLocation)
		cmd := exec.Command("/bin/sh", "-c", command)
		fmt.Printf(cmd.String() + "\n")
		_, err := cmd.CombinedOutput()
		if err != nil {
			log.Printf(err.Error())
		}
		if err != nil {
			fmt.Printf("Cant write temp file to replica %v", err)
			resp.Ack = false
			return &resp, nil
		}

		resp.Ack = true
	} else {
		log.Panicf("IDK what this case is -> file may or may not exist %v", err)
		resp.Ack = false
	}
	return &resp, nil
}

// This function is used to write a file to replica to a node
// If the file does not already exist, it create a new folder for the new file and write it.
func writeToReplica(sdfsFileName string, newLocation string, destinationLoc string, chunkArray [][]byte, putVersion string) error {
	fmt.Printf("Writing to replica %s %s %s %s\n", sdfsFileName, newLocation, destinationLoc, putVersion)
	if _, err := os.Stat(newLocation); errors.Is(err, os.ErrNotExist) {
		command := fmt.Sprintf("%s %s", peer.MKDIR_COMMAND_NAME, newLocation)
		cmd := exec.Command("/bin/sh", "-c", command)
		fmt.Printf(cmd.String() + "\n")
		_, err := cmd.CombinedOutput()
		if err != nil {
			fmt.Printf("folder create error %v", err)
			return err
		}
	}
	//Write to local file system at /replicas and don't increment the version number
	command := fmt.Sprintf("%s %s %s", peer.MV_COMMAND_NAME, "/tmp/"+sdfsFileName+".tmp", destinationLoc)
	cmd := exec.Command("/bin/sh", "-c", command)
	fmt.Printf(cmd.String() + "\n")
	out, err := cmd.CombinedOutput()
	if err != nil {
		log.Println(out, err)
	}
	return nil
}

// This is the server side implementation of the PUT rpc call
// This function at a high level performs a put call by transferring a file from the client to the /tmp/ folder
// If this is invoked at a primary, it will call replication actions on the replica nodes of this primary node
// After writing to NUM_REPLICAS, it waits on WRITE_QUORUM number of replicas to send out COMMIT RPC calls
// This initiates COMMIT rpc calls on all the replicas and waits on all of them for acks. On false acks we retry.
func (s *SDFSServer) Put(stream pb.SDFSInterface_PutServer) error {
	var chunkArray [][]byte
	var sdfsFileName string
	var canReplicate bool
	var putVersion string
	var destinationLoc string
	var versionRepair bool

	for {
		requestData, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			fmt.Printf("Error not able to stream grpc %v", err.Error())
			return err
		}
		chunkArray := requestData.GetFileChunks()
		sdfsFileName = requestData.GetSdfsFileName()
		canReplicate = requestData.GetReplicate()
		putVersion = requestData.GetPutVersion()
		versionRepair = requestData.GetVersionRepair()

		f, err := os.OpenFile("/tmp/"+sdfsFileName+".tmp", os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
		if err != nil {
			panic(err)
		}

		if _, err = f.WriteString(string(chunkArray)); err != nil {
			panic(err)
		}

		f.Close()

	}
	fmt.Printf("All params for PUT %s %v %s %v \n", sdfsFileName, canReplicate, putVersion, versionRepair)
	if _, err := os.Stat("/tmp/" + sdfsFileName); errors.Is(err, os.ErrNotExist) {
		fmt.Printf("Tmp File doesnt exist! Continue put\n")
	} else if err != nil {
		fmt.Printf("Temp file exists. blocking put for %s\n", sdfsFileName)
		return err
	}

	if putVersion != "" && !versionRepair {
		fmt.Printf("PUT REPAIR %s \n", putVersion)
		destinationLoc = "/replicas/" + sdfsFileName + "/" + sdfsFileName
		newLocation := "/replicas/" + sdfsFileName
		if _, err := os.Stat(newLocation); errors.Is(err, os.ErrNotExist) {
			command := fmt.Sprintf("%s %s", peer.MKDIR_COMMAND_NAME, newLocation)
			cmd := exec.Command("/bin/sh", "-c", command)
			_, err := cmd.CombinedOutput()
			if err != nil {
				fmt.Printf("folder create error %v", err)
			}
		}
		dir := "/replicas/" + sdfsFileName
		command := fmt.Sprintf("ls -tr1 %s", dir)
		files, err := exec.Command("/bin/sh", "-c", command).CombinedOutput()
		if err != nil {
			log.Fatal(err)
		}
		//Write to local file system at /replicas and don't increment the version number
		command = fmt.Sprintf("%s %s %s@%d", peer.MV_COMMAND_NAME, destinationLoc, destinationLoc, getVersion(sdfsFileName))
		cmd := exec.Command("/bin/sh", "-c", command)
		fmt.Printf(cmd.String() + "\n")
		out, err := cmd.CombinedOutput()
		if err != nil {
			log.Println(out, err)
		}

		files_list := strings.Split(string(files), "\n")
		if (len(files_list)) > 5 {
			todelete := files_list[0]

			fmt.Printf("Deleting extra version %s\n", todelete)
			command := fmt.Sprintf("rm /replicas/%s/%s", sdfsFileName, todelete)
			cmd := exec.Command("/bin/sh", "-c", command)
			_, err := cmd.CombinedOutput()
			if err != nil {
				fmt.Printf("delete error %v %v\n", err, todelete)
				return stream.SendAndClose(&pb.PutResponse{Ack: false})
			}
		}

		//Write to local file system at /replicas and don't increment the version number
		command = fmt.Sprintf("%s %s %s", peer.MV_COMMAND_NAME, "/tmp/"+sdfsFileName+".tmp", destinationLoc)
		cmd = exec.Command("/bin/sh", "-c", command)
		fmt.Printf(cmd.String() + "\n")
		out, err = cmd.CombinedOutput()
		if err != nil {
			log.Println(out, err)
		}

		peer.Lock.Lock()
		for i, member := range peer.MemberList {
			if member.HostName == peer.MyHostName {
				peer.MemberList[i].FileInfo[sdfsFileName], err = strconv.Atoi(putVersion)
				break
			}
		}
		peer.Lock.Unlock()
		return stream.SendAndClose(&pb.PutResponse{Ack: true})
	} else if versionRepair {
		fmt.Printf("Version REPAIR \n")
		if putVersion == "" {
			destinationLoc = "/replicas/" + sdfsFileName + "/" + sdfsFileName
		} else {
			destinationLoc = "/replicas/" + sdfsFileName + "/" + sdfsFileName + "@" + putVersion
		}
		fmt.Printf("Version REPAIR for file %s \n", destinationLoc)
		newLocation := "/replicas/" + sdfsFileName
		err := writeToReplica(sdfsFileName, newLocation, destinationLoc, chunkArray, putVersion)
		if err != nil {
			fmt.Printf("Error writing version repair %v\n", err)
			return stream.SendAndClose(&pb.PutResponse{Ack: false})
		}
		return stream.SendAndClose(&pb.PutResponse{Ack: true})
	} else {
		destinationLoc = "/tmp/" + sdfsFileName
	}

	var neighbours []string
	// Check if tmp file exists
	if _, err := os.Stat(destinationLoc); errors.Is(err, os.ErrNotExist) {
		//Check if it exists - To differentiate create vs update
		if _, err := os.Stat("/replicas/" + sdfsFileName); errors.Is(err, os.ErrNotExist) {
			fmt.Printf("File doesnt exist. Creating the file\n")
			neighbours = peer.FindNeighbours(peer.MyHostName)
			fmt.Printf("Neigh %v", neighbours)
		} else if err == nil {
			fmt.Printf("File exists. Updating the file\n")
			peer.Lock.RLock()
			for _, member := range peer.MemberList {
				if _, ok := member.FileInfo[sdfsFileName]; ok && member.HostName != peer.MyHostName {
					//File is a key in the member
					neighbours = append(neighbours, member.HostName)
				}
			}
			peer.Lock.RUnlock()
		} else {
			log.Panicf("IDK what this case is -> file may or may not exist %v", err)
		}

		//Write to local file system at /replicas and don't increment the version number
		dir := "/tmp/"
		command := fmt.Sprintf("ls -tr1 %s", dir)
		files, err := exec.Command("/bin/sh", "-c", command).CombinedOutput()
		fmt.Printf("\n\nBEFORE WRITE %s\n\n", files)
		if err != nil {
			log.Fatal(err)
		}
		command = fmt.Sprintf("%s %s %s", peer.MV_COMMAND_NAME, "/tmp/"+sdfsFileName+".tmp", destinationLoc)
		cmd := exec.Command("/bin/sh", "-c", command)
		fmt.Printf(cmd.String() + "\n")
		out, err := cmd.CombinedOutput()
		if err != nil {
			log.Println(out, err)
		}

		dir = "/tmp/"
		command = fmt.Sprintf("ls -tr1 %s", dir)
		files, err = exec.Command("/bin/sh", "-c", command).CombinedOutput()
		fmt.Printf("\n\nAFTER MV %s\n\n", files)
		if err != nil {
			log.Fatal(err)
		}
		responseChan := make(chan bool)
		if canReplicate {
			writeQuorum := 0
			fmt.Printf("I AM PRIMARY normal replicating %s %s %s\n", peer.MyHostName, sdfsFileName, destinationLoc)
			fmt.Printf("My neighbours are %v \n", neighbours)
			for _, neighbour := range neighbours {
				go replicate(neighbour, destinationLoc, sdfsFileName, responseChan)
			}
			for {
				res := <-responseChan
				if res {
					writeQuorum = writeQuorum + 1
					fmt.Printf("\n\nReceived Commit ACK\n\n")
				}
				if writeQuorum >= peer.WRITE_QUORUM {
					//Call Commit
					//add self to list of neighbours
					neighbours = append(neighbours, peer.MyHostName)
					for _, ip := range neighbours {
						CommitClient(ip, sdfsFileName)
					}
					return stream.SendAndClose(&pb.PutResponse{Ack: true})
				}
			}

		} else {
			fmt.Printf("I AM SECONDARY %s %s %s", peer.MyHostName, sdfsFileName, destinationLoc)
			return stream.SendAndClose(&pb.PutResponse{Ack: true})
		}
	} else if err != nil {
		fmt.Printf("Error. Blocking writes %s \n", err.Error())
		return stream.SendAndClose(&pb.PutResponse{Ack: false})
	}
	return stream.SendAndClose(&pb.PutResponse{Ack: false})
}

// This is the server side GET RPC call that is used to read files from the SDFS.
// This is also used by the get-versions api call to get many old versions of the file.
func (s *SDFSServer) Get(in *pb.GetRequest, stream pb.SDFSInterface_GetServer) error {
	sdfsFileName := in.GetSdfsFileName()
	numVersions := in.GetNumVersions()

	log.Println("Get request %s %d\n", in.SdfsFileName, in.NumVersions)

	dir := "/replicas/" + sdfsFileName
	command := fmt.Sprintf("ls -t1 %s", dir)
	files, err := exec.Command("/bin/sh", "-c", command).CombinedOutput()
	if err != nil {
		log.Fatal(err)
	}

	files_list := strings.Split(string(files), "\n")
	numVersions = int32(math.Min(float64(len(files_list)), float64(numVersions)))
	for i, f := range files_list[:numVersions] {
		log.Printf("getting versions %s\n\n", f)
		if i != 0 {
			version := strings.Split(f, "@")
			d := []byte(fmt.Sprintf("\n\n========== START OF %s VERSION %s ======\n\n", version[0], version[1]))
			if err := stream.Send(&pb.GetResponse{FileChunks: d}); err != nil {
				log.Fatalf("%v.Send(%v) = %v", stream, d, err)
			}
		}
		file := dir + "/" + f
		fd, err := os.Open(file)
		if err != nil {
			log.Printf("Cant read file into memory\n")
			return err
		}
		r := bufio.NewReader(fd)
		buf := make([]byte, 0, peer.CHUNK_SIZE)
		for {
			n, err := r.Read(buf[:cap(buf)])
			buf = buf[:n]
			if err := stream.Send(&pb.GetResponse{FileChunks: buf}); err != nil {
				log.Fatalf("%v.Send(%v) = %v", stream, buf, err)
			}
			if n == 0 {
				if err == nil {
					continue
				}
				if err == io.EOF {
					break
				}
				log.Fatal(err)
			}
			// process buf
			if err != nil && err != io.EOF {
				log.Fatal(err)
			}
		}
		fd.Close()
	}
	return nil
}

// The replicate function is used by the primary to replicate the put files function on all replicas
func replicate(neighbour string, localFileName string, sdfsFileName string, responseChan chan bool) {
	log.Printf("REPLICATING %s to %s from normal put call\n", sdfsFileName, neighbour)
	putRepair := ""
	response := streamAndWrite(fmt.Sprintf("%s:%d", neighbour, peer.SDFS_TCP_PORT), localFileName, sdfsFileName, false, putRepair, false)
	//log.Println(response.Ack)
	responseChan <- response.Ack
}

// This is a helper function to find primary node in the list of ips
func findPrimaryFromList(list_of_ips []string) string {
	minIP := list_of_ips[0]
	for _, ip := range list_of_ips {
		if ip < minIP {
			minIP = ip
		}
	}
	return minIP
}

// This is a helper function to find primary node in the map of ips
func findPrimaryFromMap(list_of_ips map[string]int) string {
	minIP := "9999999999999"
	for ip, _ := range list_of_ips {
		if ip < minIP {
			minIP = ip
		}
	}
	return minIP
}

// This is a helper function that is used by all put operations to start a grpc connection to a server and send files
func streamAndWrite(ip string, localFileName string, sdfsFileName string, replicate bool, putVersion string, versionRepair bool) *pb.PutResponse {
	log.Printf("All params in stream and write %s %s %s %v %s %v \n", ip, localFileName, sdfsFileName, replicate, putVersion, versionRepair)
	conn, err := grpc.Dial(ip, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Printf("did not connect: %v\n", err)
	}
	defer conn.Close()
	client := pb.NewSDFSInterfaceClient(conn)
	ctx, _ := context.WithTimeout(context.Background(), time.Second*18000)
	stream, err := client.Put(ctx)
	if err != nil {
		log.Fatalf("%v.Put(_) = _, %v", client, err)
	}

	f, err := os.Open(localFileName)
	if err != nil {
		log.Printf("Cant read file into memory\n")
		return &pb.PutResponse{Ack: false}
	}

	r := bufio.NewReader(f)
	buf := make([]byte, 0, peer.CHUNK_SIZE)
	for {
		n, err := r.Read(buf[:cap(buf)])
		buf = buf[:n]
		if err := stream.Send(&pb.PutRequest{SdfsFileName: sdfsFileName, FileChunks: buf, Replicate: replicate, PutVersion: putVersion, VersionRepair: versionRepair}); err != nil {
			log.Fatalf("%v.Send(%v) = %v", stream, buf, err)
		}
		if n == 0 {
			if err == nil {
				continue
			}
			if err == io.EOF {
				break
			}
			log.Fatal(err)
		}
		// process buf
		if err != nil && err != io.EOF {
			log.Fatal(err)
		}
	}
	reply, err := stream.CloseAndRecv()
	if err != nil {
		log.Printf("%v.CloseAndRecv() got error %v, want %v", stream, err, nil)
	}
	log.Printf("Route summary: %v\n", reply)
	return reply
}

func replaceFileName(sdfsFileName string) string {
	return strings.Replace(sdfsFileName, "/", "@", -1)
}

// This is the client side implementation of the put rpc call of the sdfs. Based on if the
// file is created for the first time or not, we chose the approriate primary node to route the put requests too.
// Further update requests are also handled by the same client.
func PutClient(localFileName string, sdfsFileName string) {
	start := time.Now()
	list_of_ips := LsClient(sdfsFileName)
	var response *pb.PutResponse
	canReplicate := true

	//Replace file name
	replacedFileName := replaceFileName(sdfsFileName)
	if len(list_of_ips) == 0 { //Create of file for first time
		log.Printf("first time create\n")
		peer.Lock.RLock()
		node_to_hash := hash(sdfsFileName) % uint32(len(peer.MemberList)) //What if this node is dead?
		node_ip := fmt.Sprintf("%s:%d", peer.MemberList[node_to_hash].HostName, peer.SDFS_TCP_PORT)
		peer.Lock.RUnlock()
		log.Printf("node_ip %s\n", node_ip)
		response = streamAndWrite(node_ip, localFileName, replacedFileName, canReplicate, "", false)
	} else { //Update of file
		primaryIP := findPrimaryFromList(list_of_ips)
		primaryIP = fmt.Sprintf("%s:%d", primaryIP, peer.SDFS_TCP_PORT)
		log.Printf("Not first time %v %s", list_of_ips, primaryIP)
		response = streamAndWrite(primaryIP, localFileName, replacedFileName, canReplicate, "", false)
	}
	if response.GetAck() {
		//Update membership list etc
		fmt.Println("Put successful")
		elapsed := time.Since(start)
		log.Printf("PUT TOOK %s\n", elapsed)
	} else {
		fmt.Println("Put unsuccessful")
	}
}

// The client side implementation of commit rpc call that is used to commit a previously put file into the sdfs.
func CommitClient(addr string, sdfsFileName string) {
	vm_addr := fmt.Sprintf("%s:%d", addr, peer.SDFS_TCP_PORT)
	conn, err := grpc.Dial(vm_addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Printf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewSDFSInterfaceClient(conn)

	version := fmt.Sprintf("%d", sdfsFileName)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*18000)
	defer cancel()
	_, err = c.Commit(ctx, &pb.CommitRequest{SdfsFileName: sdfsFileName, Version: version})
	if err != nil {
		log.Printf("could not send commit msg!: %v", err)
	}
	fmt.Printf("Committing on %v", vm_addr)
}

// The client side implementation for Get/read rpc calls. This performs a read quorum to read files from the sdfs.
// Returns error if file doesnt exist.
func GetClient(localFileName string, sdfsFileName string, numVersions int32) string {
	start := time.Now()
	var m map[int][]string
	m = make(map[int][]string)
	var ip string
	var chunkArray []byte
	r_quorum_ver := 0
	replacedFileName := replaceFileName(sdfsFileName)
	peer.Lock.RLock()
	for _, member := range peer.MemberList {
		for file, ver := range member.FileInfo {
			if file == replacedFileName {
				m[ver] = append(m[ver], member.HostName)
				if len(m[ver]) > len(m[r_quorum_ver]) {
					r_quorum_ver = ver
				}
			}
		}
	}
	peer.Lock.RUnlock()

	if len(m[r_quorum_ver]) == 0 {
		fmt.Println("FILE DOESN'T EXIST")
		return "FILE DOESN'T EXIST"
	} else if len(m[r_quorum_ver]) < peer.READ_QUORUM {
		log.Println("NO QUORUM")
		return "NO_QUORUM"
	} else {
		ip = fmt.Sprintf("%s:%d", m[r_quorum_ver][0], peer.SDFS_TCP_PORT)
	}

	conn, err := grpc.Dial(ip, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Printf("did not connect: %v\n", err)
	}

	defer conn.Close()
	client := pb.NewSDFSInterfaceClient(conn)
	ctx, _ := context.WithTimeout(context.Background(), time.Second*18000)
	stream, err := client.Get(ctx, &pb.GetRequest{SdfsFileName: sdfsFileName, NumVersions: numVersions})
	if err != nil {
		log.Panicf("%v.getRequest(_) = _, %v", client, err)
	}
	for {
		responseData, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Panicf("%v.getRequest(_) = _, %v", client, err)
		}
		chunkArray = responseData.GetFileChunks()
		f, err := os.OpenFile(localFileName, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
		if err != nil {
			panic(err)
		}

		if _, err = f.WriteString(string(chunkArray)); err != nil {
			panic(err)
		}

		f.Close()
	}

	elapsed := time.Since(start)
	log.Printf("READ TOOK %s\n", elapsed)
	return "SUCCESS"
}

// The del client is a client side RPC call for the delete function.
func DelClient(sdfsFileName string) {
	list_of_ips := LsClient(sdfsFileName)
	log.Printf("Deleting %s at %v", sdfsFileName, list_of_ips)
	primary := true
	primaryIP := findPrimaryFromList(list_of_ips)
	delClient(primaryIP, sdfsFileName, primary)
}

// The delete client function calls Delete on the primary node of the file
func delClient(hostname string, sdfsFileName string, primary bool) bool {
	vm_addr := fmt.Sprintf("%s:%d", hostname, peer.SDFS_TCP_PORT)
	log.Printf("Sending delete to VM %s\n", vm_addr)
	conn, err := grpc.Dial(vm_addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Printf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewSDFSInterfaceClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*18000)
	defer cancel()
	reply, err := c.Delete(ctx, &pb.DeleteRequest{SdfsFileName: sdfsFileName, Primary: primary})
	if err != nil {
		log.Printf("could not send delete request!: %v\n", err)
	}
	if reply.GetAck() {
		fmt.Printf("Deleted on %v\n", vm_addr)
	} else {
		fmt.Printf("Delete failed on %v\n", vm_addr)
	}
	return reply.GetAck()
}

// The LS client reads the membership list and returns the location of files
func LsClient(sdfsFileName string) []string {
	var list_of_ips []string
	fmt.Printf("%s is present on : \n", sdfsFileName)
	peer.Lock.RLock()
	for _, member := range peer.MemberList {
		for k, _ := range member.FileInfo {
			if k == sdfsFileName {
				list_of_ips = append(list_of_ips, member.HostName)
				fmt.Printf("%s\n", member.HostName)
			}
		}
	}
	peer.Lock.RUnlock()
	return list_of_ips
}

// The store function reads the membership list and lists all the files in the given node
func StoreClient() {
	fmt.Println("Files on this machine : \n")
	peer.Lock.RLock()
	for _, member := range peer.MemberList {
		if member.HostName == peer.MyHostName {
			for k, v := range member.FileInfo {
				fmt.Printf("%s:%d\n", k, v)
			}
			break
		}
	}
	peer.Lock.RUnlock()
}

// SDFS TCP GRPC Server start function
func StartSDFSServer() {
	files, err := ioutil.ReadDir(peer.REPLICAS_PATH + "/")
	if err != nil {
		log.Printf("Cant read files in directory %s\n", err)
	}
	for _, file := range files {
		err := os.RemoveAll(peer.REPLICAS_PATH + "/" + file.Name())
		if err != nil {
			log.Printf("couldnt delete replicas path %s\n", err)
		}
	}

	files, err = ioutil.ReadDir(peer.TMP_PATH + "/")
	if err != nil {
		log.Printf("Cant read files in directory %s\n", err)
	}
	for _, file := range files {
		err := os.RemoveAll(peer.TMP_PATH + "/" + file.Name())
		log.Printf("Files %s\n", file.Name())
		if err != nil {
			log.Printf("couldnt delete replicas path %s\n", err)
		}
	}

	files, err = ioutil.ReadDir(peer.LOG_DIR + "/")
	if err != nil {
		log.Printf("Cant read files in directory %s\n", err)
	}
	for _, file := range files {
		err := os.RemoveAll(peer.LOG_DIR + "/" + file.Name())
		log.Printf("Files %s\n", file.Name())
		if err != nil {
			log.Printf("couldnt delete replicas path %s\n", err)
		}
	}

	log.Println("Start SDFS GRPC Server")
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", peer.SDFS_TCP_PORT))
	if err != nil {
		log.Printf("failed to listen: %v\n", err)
		panic(err)
	}
	s := grpc.NewServer()
	pb.RegisterSDFSInterfaceServer(s, &SDFSServer{})
	log.Printf("SDFS Server listening at %v\n", lis.Addr())
	if err := s.Serve(lis); err != nil {
		log.Printf("failed to serve: %v\n", err)
	}
}
