package ml

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"mp1/peer"
	pb "mp1/protos"
	"net"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type mlServer struct {
	pb.UnimplementedMLInterfaceServer
}

// Request Structure for flask app
type flaskRequestStruct struct {
	Job_id    int
	Batch_num int
	Model     string
	Images    []string
}
type jobStruct struct {
	queries    []string
	batchSize  int
	batchQueue []int
	nextBatch  int
	model      string
}

var job1, job2 jobStruct

var mlLock sync.RWMutex
var jobId int = 0

var vmPool []string
var vmToJobMap map[string]int
var vmToBatchMap map[string]int

// Helper to check if current vm is primary coordinator
func amPrimary() bool {
	fmt.Println(peer.MyHostName)
	fmt.Println(peer.CoordinatorAddr)
	fmt.Println(peer.MyHostName == peer.CoordinatorAddr)
	return peer.MyHostName == peer.CoordinatorAddr
}

// Helper to check if current vm is standby coordinator
func amStandby() bool {
	return peer.MyHostName == peer.StandbyCoordinatorAddr
}

// API call to check query rate of a job
func QueryRate(job_id string) error {
	response, err := http.Get("http://127.0.0.1:5000/query-rate?job-id=" + job_id)
	if err != nil {
		fmt.Println(err)
		return err
	}
	fmt.Println(response)
	return nil
}

// API call to get query stats of a a job
func QueryProcessing(job_id string) error {
	response, err := http.Get("http://127.0.0.1:5000/query-processing?job-id=" + job_id)
	if err != nil {
		fmt.Println(err)
		return err
	}
	fmt.Println(response)
	return nil
}

// API call to set batch size of a job
func SetBatchSize(batch_size int, jobId int) error {
	//Set Batch Size
	if jobId == 1 {
		job1.batchSize = batch_size
	} else {
		job2.batchSize = batch_size
	}
	return nil
}

// API call to get results of inferencing
func GetResults(job_id string) error {
	//Get Results
	response, err := http.Get("http://127.0.0.1:5000/get-results?job-id=" + job_id)
	if err != nil {
		fmt.Println(err)
		return err
	}
	fmt.Println(response)

	return nil
}

// API call to show vm to job association
func ShowVMS(job_id int) error {
	//Show VMS
	var out []string
	for vm, job := range vmToJobMap {
		if job == job_id {
			out = append(out, vm)
		}
	}
	fmt.Printf("%v", vmToJobMap)
	return nil
}

// Async function to kill python process
func goKill() {
	fmt.Println("IN GOKILL")
	vmAddr := fmt.Sprintf("http://%s:5000/kill", peer.MyHostName)
	_, err := http.Get(vmAddr)
	if err != nil {
		log.Println(err)
	}
}

// Helper function to kill the IDunno server
func killIdunno() error {
	fmt.Println("Killing IDunno")
	go goKill()
	fmt.Println("Killed Idunno Successfully!")
	return nil
}

// Start the IDunno go grpc server
func StartIdunno(flag int) error {
	if flag != 0 {
		killIdunno()
	}
	time.Sleep(10 * time.Second)
	fmt.Println("Starting IDunno")
	command := fmt.Sprintf("~/dist/venv/bin/gunicorn -w 2 -b 0.0.0.0:5000 server:app --chdir models/  --log-level debug --access-logfile access.log --error-logfile %s.log --daemon", peer.MyHostName)
	cmd := exec.Command("/bin/sh", "-c", command)
	output, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Println("Didnt Started Idunno Successfully :(")
		fmt.Println(err.Error())
		fmt.Println(string(output))
		return err
	}
	fmt.Println("Started Idunno Successfully!")
	return nil
}

// RPC call to kill the python server
func (s *mlServer) Kill(ctx context.Context, in *pb.KillRequest) (*pb.KillResponse, error) {
	var resp pb.KillResponse
	resp.HostName = peer.MyHostName
	fmt.Println(11)
	StartIdunno(1)
	fmt.Println(12)
	fmt.Println(16)
	files, err := filepath.Glob("models/timestamps-" + peer.MyHostName)
	if err != nil {
		return &resp, err
	}
	fmt.Println(17)
	for _, f := range files {
		if err := os.Remove(f); err != nil {
			return &resp, err
		}
	}
	fmt.Println(18)
	resp.HostName = peer.MyHostName
	return &resp, nil
}

var cnt int = 1

// Helper function to send async post request
func asyncPostRequest(flaskRequest *flaskRequestStruct) error {
	json_data, err := json.Marshal(&flaskRequest)
	if err != nil {
		fmt.Printf("Error json marshal %v \n", err)
		return err
	}
	log.Println(string(json_data))
	if err != nil {
		log.Fatal(err)
	}
	client := &http.Client{
		Timeout: 60 * time.Second,
	}
	req, err := http.NewRequest(http.MethodPost, "http://localhost:5000/predict", bytes.NewBuffer(json_data))
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		return err
	}
	ctx, cancel := context.WithTimeout(req.Context(), 60*time.Second)
	defer cancel()
	req = req.WithContext(ctx)
	_, err = client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	return nil
}

// Server side RPC to schedule a job on VM
func (s *mlServer) ScheduleJob(ctx context.Context, in *pb.ScheduleRequest) (*pb.ScheduleResponse, error) {
	fmt.Printf("In schedule job\n")
	var flaskRequest flaskRequestStruct
	for _, file := range in.FileNames {
		//sdfs.GetClient(file, file, 1)
		flaskRequest.Images = append(flaskRequest.Images, file)
	}
	flaskRequest.Job_id = int(in.JobId)
	flaskRequest.Batch_num = int(in.BatchNum)
	flaskRequest.Model = in.ModelName
	x := cnt
	cnt += 1
	fmt.Printf("Sending post %d %d\n", x, cnt)
	go asyncPostRequest(&flaskRequest)
	return &pb.ScheduleResponse{}, nil
}

// Server side RPC to respond an ack after a job is completed
func (s *mlServer) SendAckLocal(ctx context.Context, in *pb.SendAckMsg) (*pb.SendAckMsg, error) {
	vmAddr := fmt.Sprintf("%s:%d", peer.CoordinatorAddr, peer.ML_TCP_PORT)
	fmt.Println("RECEIVED SEND ACK LOCAL", vmAddr)
	conn, err := grpc.Dial(vmAddr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		fmt.Printf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewMLInterfaceClient(conn)

	_, err = c.SendAck(ctx, &pb.SendAckRequest{VmAddr: peer.MyHostName})
	if err != nil {
		log.Printf("could not send job request!: %v", err)
	}

	if peer.CoordinatorAddr != peer.StandbyCoordinatorAddr {
		vmAddr = fmt.Sprintf("%s:%d", peer.StandbyCoordinatorAddr, peer.ML_TCP_PORT)
		fmt.Println("RECEIVED SEND ACK LOCAL", vmAddr)
		conn, err = grpc.Dial(vmAddr, grpc.WithTransportCredentials(insecure.NewCredentials()))
		if err != nil {
			fmt.Printf("did not connect: %v", err)
		}
		defer conn.Close()
		c = pb.NewMLInterfaceClient(conn)

		_, err = c.SendAck(ctx, &pb.SendAckRequest{VmAddr: peer.MyHostName})
		if err != nil {
			log.Printf("could not send job request!: %v", err)
		}
	}

	return &pb.SendAckMsg{}, nil
}

// Server side RPC to send an ack to the coordinator once the job is complete
func (s *mlServer) SendAck(ctx context.Context, in *pb.SendAckRequest) (*pb.SendAckResponse, error) {
	fmt.Println("RECEIVED SEND ACK COORD")
	jobToDo := -1
	batchNum := -1
	var batchStart int
	var batchEnd int
	var i int
	in.VmAddr = strings.TrimSuffix(in.VmAddr, "\n")
	fmt.Printf("Received ack %s,%v %d\n", in.VmAddr, vmToJobMap, vmToJobMap[in.VmAddr])
	//make locking finer
	if vmToJobMap[in.VmAddr] == 1 {
		fmt.Println("in job1 ")
		if len(job1.queries) == 0 {
			fmt.Println("in query 0 ")
			if len(job2.queries) != 0 {
				jobToDo = 2
			}
			//add rpc to send job comp msg to client
		} else {
			batchNum = job1.nextBatch
			jobToDo = 1
		}
	} else if vmToJobMap[in.VmAddr] == 2 {
		if len(job2.queries) == 0 {
			if len(job1.queries) != 0 {
				jobToDo = 1
			}
			//add rpc to send job comp msg to client
		} else {
			batchNum = job2.nextBatch
			jobToDo = 2
		}

	} else {
		log.Panicln("Expected Job on vm but doesn't exist")
	}

	if len(job1.batchQueue) == 0 {
		for i = job1.nextBatch; i < job1.nextBatch+job1.batchSize; i++ {
			job1.batchQueue = append(job1.batchQueue, i)
		}
		job1.nextBatch = i
	}
	if len(job2.batchQueue) == 0 {
		for i = job2.nextBatch; i < job2.nextBatch+job2.batchSize; i++ {
			job2.batchQueue = append(job2.batchQueue, i)
		}
		job2.nextBatch = i
	}
	if jobToDo != -1 {
		batchSize := 1
		var queue []string
		var model string
		if jobToDo == 1 {
			batchSize = job1.batchSize
			queue = job1.queries
			model = job1.model
			batchNum = job1.batchQueue[0]
			job1.batchQueue = job1.batchQueue[1:]
		} else if jobToDo == 2 {
			batchSize = job2.batchSize
			queue = job2.queries
			model = job2.model
			batchNum = job2.batchQueue[0]
			job2.batchQueue = job2.batchQueue[1:]
		}
		batchStart = batchNum * batchSize
		batchEnd = batchStart + batchSize
		var out []string
		fmt.Println("send ack out append job", jobToDo, "batch s e", batchStart, batchEnd, "queue", len(queue))
		for j := batchStart; j < batchEnd; j++ {
			out = append(out, queue[j%len(queue)])
		}

		vm_addr := fmt.Sprintf("%s:%d", in.VmAddr, peer.ML_TCP_PORT)
		vmToJobMap[in.VmAddr] = int(jobToDo)
		vmToBatchMap[in.VmAddr] = batchNum

		fmt.Printf("Sending scheduleJob request to Worker VM in sendack %s\n", vm_addr)
		conn, err := grpc.Dial(vm_addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
		if err != nil {
			fmt.Printf("did not connect: %v", err)
		}
		defer conn.Close()
		c := pb.NewMLInterfaceClient(conn)

		if amPrimary() {
			_, err = c.ScheduleJob(ctx, &pb.ScheduleRequest{ModelName: model, FileNames: out, JobId: int32(jobToDo), BatchNum: int32(batchNum)})
			if err != nil {
				log.Printf("could not send job request!: %v", err)
			}
		}

	} else {
		vmPool = append(vmPool, in.VmAddr)
		vmToJobMap[in.VmAddr] = int(jobToDo)
		vmToBatchMap[in.VmAddr] = batchNum
	}
	fmt.Println("SUCCESSFUL SENDACK RET")
	return &pb.SendAckResponse{}, nil
}

// Server side RPC to request running of a job on the cluster to the coordinator
func (s *mlServer) RequestJob(ctx context.Context, in *pb.JobRequest) (*pb.JobResponse, error) {
	fmt.Printf("In request job\n")
	var i int
	var batchNum int
	mlLock.Lock()
	jobId += 1
	reqJobId := jobId
	mlLock.Unlock()
	if reqJobId == 1 {
		job1.queries = in.FileNames
		job1.model = in.ModelName

		fmt.Printf("appending to batchq abc %v %v", job1.queries, in.FileNames)
		job1.batchSize = int(in.BatchSize)
	} else {
		job2.queries = in.FileNames
		job2.model = in.ModelName
		fmt.Printf("appending to batchq abcd %v %v", job2.queries, in.FileNames)
		job2.batchSize = int(in.BatchSize)
	}

	for i = 0; i/int(in.BatchSize) < (len(peer.AddrsPlain)+1)/2; i += int(in.BatchSize) {
		fmt.Printf("In request loop\n")
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*18000)
		defer cancel()
		var out []string
		for j := i; j < i+int(in.BatchSize); j++ {
			out = append(out, in.FileNames[j%len(in.FileNames)])
		}
		fmt.Printf("In request job 2\n")
		fmt.Printf("VMPOOL %v", vmPool)

		vm_addr := fmt.Sprintf("%s:%d", vmPool[0], peer.ML_TCP_PORT)
		vmToJobMap[vmPool[0]] = reqJobId
		vmToBatchMap[vmPool[0]] = i / int(in.BatchSize)
		//not adding logic using idx to check if vm exists in memberlist
		vmPool = vmPool[1:]

		fmt.Printf("Sending scheduleJob request to Worker VM %s\n", vm_addr)
		conn, err := grpc.Dial(vm_addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
		if err != nil {
			fmt.Printf("did not connect: %v", err)
		}
		defer conn.Close()

		c := pb.NewMLInterfaceClient(conn)

		if amPrimary() {
			fmt.Printf("Im primary so scheduling now%s\n", vm_addr)
			_, err = c.ScheduleJob(ctx, &pb.ScheduleRequest{ModelName: in.ModelName, FileNames: out, BatchNum: int32(batchNum), JobId: int32(reqJobId)})
			if err != nil {
				log.Printf("could not send job request! %v", err)
			}
		}

	}
	if reqJobId == 1 {
		job1.nextBatch = i / int(in.BatchSize)
	} else {
		job2.nextBatch = i / int(in.BatchSize)
	}
	fmt.Printf("after req jq %v\n", job1.batchQueue)
	return &pb.JobResponse{}, nil
}

// Server side RPC to sync backup and primary coordinator
func (s *mlServer) SyncBackupServer(ctx context.Context, in *pb.SyncRequest) (*pb.SyncResponse, error) {
	if in.JobId == 1 {
		job2.batchQueue = append(job2.batchQueue, int(in.KilledBatch))
		job1.batchQueue = job1.batchQueue[1:]
	} else {
		job1.batchQueue = append(job1.batchQueue, int(in.KilledBatch))
		job2.batchQueue = job2.batchQueue[1:]
	}
	vmToJobMap[in.HostName] = int(in.JobId)
	vmToBatchMap[in.HostName] = int(in.BatchNum)

	return &pb.SyncResponse{}, nil
}

// The main query rate fixing logic. This tries to perform fair time scheduling
// by preempting jobs that have higher query rates and replacing them with lower query rates
func FixQueryRate() {
	ticker := time.NewTicker(3000 * time.Millisecond)
	var job1QueryRate float64 = 0
	var job2QueryRate float64 = 0
	//var memberList []peer.Member
	var queue []string
	var vmAddr string
	var jobToDo int
	var host string
	var batchSize int
	var model string
	for _ = range ticker.C {
		job1QueryRate = 0
		job2QueryRate = 0
		peer.Lock.RLock()
		memberList := make([]peer.Member, len(peer.MemberList))
		copy(memberList, peer.MemberList)
		peer.Lock.RUnlock()
		fmt.Printf("memlist %v\n\n", memberList)
		flag := false
		for _, member := range memberList {
			if member.Status == 0 {
				continue
			}
			vmAddr := fmt.Sprintf("http://%s:5000/query-rate", member.HostName)
			//fmt.Printf("Querying %s\n", vmAddr)
			resp, err := http.Get(vmAddr)
			if err != nil {
				log.Println(err)
				flag = true
				break
			}
			body, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				log.Println(err)
				flag = true
				break
			}
			//fmt.Printf("RESP BODY %v\n", string(body))
			rate, _ := strconv.ParseFloat(string(body), 64)
			if rate == -1 {
				flag = true
				break
			}
			if vmToJobMap[member.HostName] == 1 {
				job1QueryRate += rate
			} else if vmToJobMap[member.HostName] == 2 {
				job2QueryRate += rate
			}
		}
		fmt.Printf("==========J1 Query rate %v\n\n", job1QueryRate)
		fmt.Printf("==========J2 Query rate %v\n\n", job2QueryRate)
		if jobId != 2 {
			continue
		}
		fmt.Println(1)
		if flag {
			continue
		}
		fmt.Println(2)

		den := math.Max(job1QueryRate, job2QueryRate)
		if math.Abs(job1QueryRate-job2QueryRate)/den > 0.2 {
			if job1QueryRate == den {
				for _, member := range memberList {
					if member.Status != 0 && vmToJobMap[member.HostName] == 1 {
						vmAddr = fmt.Sprintf("%s:%d", member.HostName, peer.ML_TCP_PORT)
						host = member.HostName
						break
					}
				}
			} else {
				for _, member := range memberList {
					if member.Status != 0 && vmToJobMap[member.HostName] == 2 {
						vmAddr = fmt.Sprintf("%s:%d", member.HostName, peer.ML_TCP_PORT)
						host = member.HostName
						break
					}
				}
			}
			fmt.Println(3)
			conn, err := grpc.Dial(vmAddr, grpc.WithTransportCredentials(insecure.NewCredentials()))
			if err != nil {
				fmt.Printf("did not connect: %v", err)
			}
			defer conn.Close()
			fmt.Println(4)
			c := pb.NewMLInterfaceClient(conn)
			ctx, cancel := context.WithTimeout(context.Background(), time.Second*18000)
			defer cancel()
			fmt.Println(5, "killing job", vmToJobMap[host], "host", host)
			//add batchnum to killresponse
			killResp, err := c.Kill(ctx, &pb.KillRequest{})
			if err != nil {
				fmt.Println("PLS HELP", err)
			}
			fmt.Println(6)
			var batchNum int
			i := 0
			if len(job1.batchQueue) == 0 {
				for i = job1.nextBatch; i < job1.nextBatch+job1.batchSize; i++ {
					job1.batchQueue = append(job1.batchQueue, i)
				}
				job1.nextBatch = i
			}
			if len(job2.batchQueue) == 0 {
				for i = job2.nextBatch; i < job2.nextBatch+job2.batchSize; i++ {
					job2.batchQueue = append(job2.batchQueue, i)
				}
				job2.nextBatch = i
			}
			if job1QueryRate == den {
				job1.batchQueue = append(job1.batchQueue, vmToBatchMap[killResp.HostName])
				fmt.Printf("q1 %v\n\n q2 %v\n\n", job1.batchQueue, job2.batchQueue)
				batchNum = job2.batchQueue[0]
				job2.batchQueue = job2.batchQueue[1:]
				queue = job2.queries
				jobToDo = 2
				model = job2.model
				batchSize = job2.batchSize
			} else {
				job2.batchQueue = append(job2.batchQueue, vmToBatchMap[killResp.HostName])
				fmt.Printf("q1 %v\n\n q2 %v\n\n", job1.batchQueue, job2.batchQueue)
				batchNum = job1.batchQueue[0]
				job1.batchQueue = job1.batchQueue[1:]
				queue = job1.queries
				jobToDo = 1
				model = job1.model
				batchSize = job1.batchSize
			}
			killedBatch := vmToBatchMap[killResp.HostName]
			vmToJobMap[host] = -1
			vmToBatchMap[host] = -1
			batchStart := batchNum * batchSize
			batchEnd := batchStart + batchSize
			var out []string
			for j := batchStart; j < batchEnd; j++ {
				out = append(out, queue[j%len(queue)])
			}
			vmToJobMap[host] = jobToDo
			vmToBatchMap[host] = batchNum
			fmt.Println("scheduling job", vmToJobMap[host], "host", host)
			fmt.Println(7)

			conn, err = grpc.Dial(vmAddr, grpc.WithTransportCredentials(insecure.NewCredentials()))
			if err != nil {
				fmt.Printf("did not connect: %v", err)
			}
			fmt.Println(8)
			defer conn.Close()
			c = pb.NewMLInterfaceClient(conn)
			ctx, cancel = context.WithTimeout(context.Background(), time.Second*18000)
			defer cancel()
			fmt.Println(9)

			_, err = c.ScheduleJob(ctx, &pb.ScheduleRequest{ModelName: model, FileNames: out, BatchNum: int32(batchNum), JobId: int32(jobToDo)})
			fmt.Println(10)
			if err != nil {
				log.Printf("could not send job request!: %v", err)
			}

			if peer.CoordinatorAddr != peer.StandbyCoordinatorAddr {
				conn, err = grpc.Dial(peer.StandbyCoordinatorAddr, grpc.WithTransportCredentials(insecure.NewCredentials()))
				if err != nil {
					fmt.Printf("did not connect: %v", err)
				}
				defer conn.Close()
				c = pb.NewMLInterfaceClient(conn)
				ctx, cancel = context.WithTimeout(context.Background(), time.Second*18000)
				defer cancel()

				_, err = c.SyncBackupServer(ctx, &pb.SyncRequest{BatchNum: int32(batchNum), JobId: int32(jobToDo), HostName: killResp.HostName, KilledBatch: int32(killedBatch)})
				if err != nil {
					log.Printf("could not send job request!: %v", err)
				}
			}
			time.Sleep(15 * time.Second)
		}
	}
	fmt.Printf("Outside TICKER FAILED")
}

// Starting ML Server
func StartMLServer() {
	if amPrimary() || amStandby() {
		vmToJobMap = make(map[string]int)
		vmToBatchMap = make(map[string]int)
		fmt.Printf("jmap %v bmap %v", vmToJobMap, vmToBatchMap)
	}
	files, err := filepath.Glob("models/*.log")
	if err != nil {
		fmt.Printf("failed to delete: %v\n", err)
	}
	fmt.Println(13)
	for _, f := range files {
		fmt.Printf(f)
		if err := os.Remove(f); err != nil {
			fmt.Printf("failed to delete: %v\n", err)
		}
	}
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", peer.ML_TCP_PORT))
	if err != nil {
		fmt.Printf("failed to listen: %v\n", err)
		panic(err)
	}
	for _, member := range peer.AddrsPlain {
		vmPool = append(vmPool, member)
	}
	s := grpc.NewServer()
	pb.RegisterMLInterfaceServer(s, &mlServer{})
	fmt.Printf("VMPOOL %v", vmPool)
	if amPrimary() {
		go FixQueryRate()
	}
	if err := s.Serve(lis); err != nil {
		fmt.Printf("failed to serve: %v\n", err)
	}
	fmt.Printf("OUT startMLServer and serving on :%d\n", peer.ML_TCP_PORT)
}

func RequestJob(job_file string, model_name string, batch_size int) error {
	vm_addr := fmt.Sprintf("%s:%d", peer.CoordinatorAddr, peer.ML_TCP_PORT)
	fmt.Printf("Sending requestJob to Coordinator %s\n", vm_addr)
	conn, err := grpc.Dial(vm_addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		fmt.Printf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewMLInterfaceClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*18000)
	defer cancel()
	fd, err := os.Open(job_file)
	if err != nil {
		fmt.Printf("Cant read file into memory\n")
		return err
	}
	fileScanner := bufio.NewScanner(fd)
	fileScanner.Split(bufio.ScanLines)
	var files []string

	for fileScanner.Scan() {
		files = append(files, fileScanner.Text())
	}

	jobReq := pb.JobRequest{ModelName: model_name, BatchSize: int32(batch_size), FileNames: files}
	_, err = c.RequestJob(ctx, &jobReq)
	if err != nil {
		log.Printf("could not send job request!: %v", err)
	}

	if peer.CoordinatorAddr != peer.StandbyCoordinatorAddr {
		vm_addr = fmt.Sprintf("%s:%d", peer.StandbyCoordinatorAddr, peer.ML_TCP_PORT)
		fmt.Printf("Sending requestJob to Coordinator %s\n", vm_addr)
		conn, err = grpc.Dial(vm_addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
		if err != nil {
			fmt.Printf("did not connect: %v", err)
		}
		defer conn.Close()
		c = pb.NewMLInterfaceClient(conn)

		ctx, cancel = context.WithTimeout(context.Background(), time.Second*18000)
		defer cancel()

		jobReq = pb.JobRequest{ModelName: model_name, BatchSize: int32(batch_size), FileNames: files}
		_, err = c.RequestJob(ctx, &jobReq)
		if err != nil {
			log.Printf("could not send job request!: %v", err)
		}
	}
	return nil
}
