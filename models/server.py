from flask import Flask, redirect, url_for, request
import resnet
import alexnet
import time
import numpy as np
from flask import jsonify
import os
from bisect import bisect_left
import grpc
import logging
import ml_pb2_grpc
import ml_pb2
from multiprocessing import Process
import os
from os import environ

# environ["GRPC_DNS_RESOLVER"] = "native"

app = Flask(__name__)
timestamps = []
hostname = ""
@app.before_first_request
def before_first_request():
    app.logger.error("Setting my hostname")
    app.logger.debug("Setting my hostname")
    print("Setting my hostname")
    content = ""
    with open("/mp1/hostname", 'r') as file:
        content = file.readlines()
        hostname = content[0].strip('\n')
    app.logger.debug(hostname.strip("\n"))

coordinator = "172.22.94.8:50056"
standby_coordinator = "172.22.156.9:50056"


def run_model(job_id, batch_num,imgs,model):
    global hostname
    file_name = str(job_id) + "-" + str(batch_num)+'.log'
    job_file_name = "timestamps-" + hostname+'.log'
    app.logger.debug(job_file_name)
    with open(file_name, 'a+') as file, open(job_file_name,'a+') as file2:  # Use file to refer to the file object
        for img in imgs:
            if model == "resnet":
                category = resnet.predict_resnet(img)
                t = time.time()
                line = str(t) + " " + category + os.linesep
                file.write(line)
                file2.write(str(t)+ os.linesep)
            elif model == "alexnet":
                category = alexnet.predict_alexnet(img)
                t = time.time()
                line = str(t) + " " + category + os.linesep
                file.write(line)
                file2.write(str(t)+ os.linesep)
    app.logger.debug('model run complete')
    with grpc.insecure_channel('{}:{}'.format('localhost','50056')) as channel:
        stub = ml_pb2_grpc.MLInterfaceStub(channel)
        response = stub.SendAckLocal(ml_pb2.SendAckMsg())
        app.logger.debug('ACK RECEIVED')

@app.route('/predict', methods = ['POST'])
def predict():
    app.logger.debug("in predict deb")
    content_type = request.headers.get('Content-Type')
    if (content_type == 'application/json'):
        json_data = request.json
        imgs = json_data['Images']
        job_id = json_data['Job_id']
        batch_num = json_data['Batch_num']
        model = json_data['Model']
        p = Process(target=run_model, args=(job_id,batch_num,imgs,model))
        p.start()

    return 'SUCCESS'

@app.route('/query-rate', methods = ['GET'])
def query_rate():
    t = []
    job_file_name = "timestamps-" + hostname +'.log'
    app.logger.debug("in query rate")
    with open(job_file_name,'r') as file:
        for y in file.readlines():
            t.append(float(y))
    app.logger.debug('length %d',len(t))
    n = len(t)
    if(n<15):
        return str(-1)
    oldt = time.time() - 5
    i = bisect_left(t, oldt)
    app.logger.debug("QUERY RATE %d",i)
    if i:
        queries_processed = n - (i-1)
        return str(queries_processed/5)
    else:
#         return str(n/10)
        return str(0)

@app.route('/query-processing', methods = ['POST'])
def query_processing():
    content_type = request.headers.get('Content-Type')
    json = request.json
    if(len(timestamps)>3):
        current_time = timestamps[-1] - timestamps[-2]
        times = []
        for i in range(len(timestamps)-1,0,-1):
            times.append(timestamps[i]-timestamps[i-1])
        tarray = np.array(times)
        q1 = np.percentile(times,25)
        q2 = np.percentile(times,50)
        q3 = np.percentile(times,75)
        mean = np.mean(times)
        std = np.std(times)
        return jsonify(q1=q1,q2=q2,q3=q3,mean=mean,std=std,current_time=current_time)
    else:
        return 'Error no predictions made!'

@app.route('/kill', methods = ['GET'])
def kill():
    os.system("pkill -SIGKILL gunicorn")
    return "SUCCESS"

if __name__ != '__main__':
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)
    app.logger.debug("flask up")

    app.logger.debug("Setting my hostname")
    content = ""
    with open("/mp1/hostname", 'r') as file:
        content = file.readlines()
        hostname =  content[0].strip('\n')
    app.logger.debug(hostname.strip("\n"))

if __name__ == '__main__':
    app.run(host="0.0.0.0",debug = True,threaded=True)