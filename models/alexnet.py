def predict_alexnet(image):
    from torchvision import models
    import torch

    model = models.alexnet(pretrained=False)
    state_dict = torch.load("alexnet-owt-7be5be79.pth", map_location='cpu')
    model.load_state_dict(state_dict, strict=False)
    model.eval()

    from torchvision import transforms
    transform = transforms.Compose([
     transforms.Resize(256),
     transforms.CenterCrop(224),
     transforms.ToTensor(),
     transforms.Normalize(
     mean=[0.485, 0.456, 0.406],
     std=[0.229, 0.224, 0.225]
     )])

    from PIL import Image
    img = Image.open(image)
    img_t = transform(img)
    batch_t = torch.unsqueeze(img_t, 0)

    # forward pass
    out = model(batch_t)

    with open("imagenet_classes.txt") as f:
        classes = [line.strip() for line in f.readlines()]

    _, index = torch.max(out, 1)
    percentage = torch.nn.functional.softmax(out, dim=1)[0] * 100
    print(classes[index[0]], percentage[index[0]].item())
    return classes[index[0]]

# predict_alexnet("cat.jpg")