def predict_resnet(image):
    print("Predicting resnet")
    import torchvision
    import torch
    from torchvision import models
    from torchvision.io import read_image
    from torchvision.models import resnet50,  alexnet

    model = models.resnet50(pretrained=False)
    state_dict = torch.load("resnet50-0676ba61.pth", map_location='cpu')
    model.load_state_dict(state_dict, strict=False)
    model.eval()

    from torchvision import transforms
    transform = transforms.Compose([
     transforms.Resize(256),
     transforms.CenterCrop(224),
     transforms.ToTensor(),
     transforms.Normalize(
     mean=[0.485, 0.456, 0.406],
     std=[0.229, 0.224, 0.225]
     )])

    from PIL import Image
    img = Image.open(image)
    img_t = transform(img)
    batch_t = torch.unsqueeze(img_t, 0)

    # forward pass
    out = model(batch_t)
    with open("imagenet_classes.txt") as f:
      classes = [line.strip() for line in f.readlines()]

    # Forth, print the top 5 classes predicted by the model
    _, indices = torch.sort(out, descending=True)
    percentage = torch.nn.functional.softmax(out, dim=1)[0] * 100
    print([(classes[idx], percentage[idx].item()) for idx in indices[0][:1]])
    category = [classes[idx] for idx in indices[0][:1]][0]
    return category

# predict_resnet("cat.jpg")