// Package main implements the peer and introducer for a failure detector system.
// NOTE: logs of kind RL/L/U/RU with indices are to check if a Lock has been acquired/released at
// a particular point
package peer

import (
	"bufio"
	"bytes"
	"context"
	"encoding/gob"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"sort"
	"sync"
	"time"

	"github.com/chrusty/go-tableprinter"

	pb "mp1/protos"

	"golang.org/x/exp/slices"
	"google.golang.org/grpc"
)

var vm_addrs = [10]string{"172.22.94.8:50053", "172.22.156.9:50053", "172.22.158.9:50053", "172.22.94.9:50053", "172.22.156.10:50053",
	"172.22.158.10:50053", "172.22.94.10:50053", "172.22.156.11:50053", "172.22.158.11:50053", "172.22.94.11:50053"}

var vm_addrs_plain = [8]string{"172.22.158.9", "172.22.94.9", "172.22.156.10", "172.22.158.10", "172.22.94.10", "172.22.156.11", "172.22.158.11", "172.22.94.11"}
var vm_addrs_plain_dup = [2]string{"172.22.158.9", "172.22.94.9"}

var rahul_addrs = [9]string{"192.168.64.13:50053", "192.168.64.14:50053", "192.168.64.18:50053", "192.168.64.16:50053", "192.168.64.17:50053", "192.168.64.19:50053",
	"192.168.64.20:50053", "192.168.64.21:50053", "192.168.64.22:50053"}

// var pkk_addrs = [10]string{"192.168.64.14:50053", "192.168.64.15:50053", "192.168.64.16:50053", "192.168.64.17:50053", "192.168.64.18:50053", "192.168.64.19:50053",
//
//	"192.168.64.20:50053", "192.168.64.22:50053", "192.168.64.23:50053"}
//
// var pkk_addrs_plain = [10]string{"192.168.64.14", "192.168.64.15", "192.168.64.16", "192.168.64.17", "192.168.64.18", "192.168.64.19",
//
//	"192.168.64.20", "192.168.64.22", "192.168.64.23"}
var pkk_addrs = [9]string{"192.168.64.14:50053", "192.168.64.15:50053", "192.168.64.16:50053", "192.168.64.17:50053", "192.168.64.18:50053", "192.168.64.19:50053",
	"192.168.64.20:50053", "192.168.64.22:50053", "192.168.64.23:50053"}
var pkk_addrs_plain = [2]string{"192.168.64.16", "192.168.64.17"}
var pkk_addrs_plain_loner = [1]string{"192.168.64.14"}

const (
	TCP_PORT            = 50053
	UDP_PORT_LISTEN     = 50054
	UDP_PORT_DIAL       = 0
	LOG_DIR             = "/mp1/logs/"
	SDFS_TCP_PORT       = 50055
	ML_TCP_PORT         = 50056
	NUM_NODES           = 10
	CHUNK_SIZE          = 4000000
	WRITE_QUORUM        = 3 //excluding primary -> therefore write quorom is 4
	READ_QUORUM         = 3
	NUM_REPLICAS        = 5
	MV_COMMAND_NAME     = "mv"
	MKDIR_COMMAND_NAME  = "mkdir"
	DELETE_COMMAND_NAME = "rm -rf"
	REPLICAS_PATH       = "/replicas"
	TMP_PATH            = "/tmp"
)

type server struct {
	pb.UnimplementedIntroInterfaceServer
}

// Member structure which is part of the membership list, this structure has
// timers which are important for the suspiciion mechanism timeouts
type Member struct {
	HostName    string
	Timestamp   int
	Status      int //0 -> dead 1 -> sus 2 -> alive
	Incarnation int
	Timer       *time.Timer
	FileInfo    map[string]int
}

// trimmed down version of the Member structure that doesn't contain timers,
// to be sent over udp
type UDPmember struct {
	HostName    string
	Timestamp   int
	Status      int //0 -> dead 1 -> sus 2 -> alive
	Incarnation int
	FileInfo    map[string]int
}

// msg struct exchanged between udp-server and the client, containing memberlist
type UDPMsg struct {
	MemberList []UDPmember
}

// variable to switch log dir location when using local machinevs vms
var Addrs = pkk_addrs
var AddrsPlain = pkk_addrs_plain

var CoordinatorAddr = "192.168.64.14"
var StandbyCoordinatorAddr = "192.168.64.15"

// variable to store peers ipaddr
var MyHostName string

// membership list that each peer maintains
var MemberList []Member = make([]Member, 0, 20)

// read-write mutex to Lock access to membership list, which can be used by
// multiple threads
var Lock sync.RWMutex
var leaving = false

// function to find the neighbours of a given host
func FindNeighbours(hostname string) []string {
	var returnList []string
	Lock.RLock()
	n := len(MemberList)
	idx := slices.IndexFunc(MemberList, func(c Member) bool { return c.HostName == hostname })
	if n != 0 && (idx+1)%n != idx {
		returnList = append(returnList, MemberList[(idx+1)%n].HostName)
	}
	if n != 0 && (idx+n-1)%n != idx && MemberList[(idx+n-1)%n].HostName != returnList[0] {
		returnList = append(returnList, MemberList[(idx+n-1)%n].HostName)
	}
	idx2 := slices.IndexFunc(returnList, func(c string) bool { return c == MemberList[(idx+n/2)%n].HostName })
	if n != 0 && idx != (idx+n/2)%n && idx2 == -1 {
		returnList = append(returnList, MemberList[(idx+n/2)%n].HostName)
	}
	Lock.RUnlock()
	return returnList
}

// fucntion to convert Member struct info to protobuf peer struct
func toMemberMsg(idx int, n int) *pb.Peer {
	return &pb.Peer{Hostname: MemberList[(idx)%n].HostName,
		Timestamp:   int64(MemberList[(idx)%n].Timestamp),
		Status:      int64(MemberList[(idx)%n].Status),
		Incarnation: int64(MemberList[(idx)%n].Incarnation)}
}

// function to convert protobuf peer array into membership list
func ToMemberList(inPeers []*pb.Peer) []Member {
	var members []Member
	for _, ele := range inPeers {
		var fileInfoMap map[string]int
		json.Unmarshal([]byte(ele.FileInfo), &fileInfoMap)
		members = append(members, Member{
			ele.Hostname, int(ele.Timestamp), int(ele.Status), int(ele.Incarnation), nil, fileInfoMap,
		})
	}
	return members
}

// function to calculate the members to be given by the introducer to the
// incoming peer requesting peers
func introNeighbours(hostname string) []*pb.Peer {
	var peers []*pb.Peer
	Lock.RLock()
	n := len(MemberList)
	idx := slices.IndexFunc(MemberList, func(c Member) bool { return c.HostName == hostname })
	peers = append(peers, toMemberMsg(idx, n))
	idx2 := slices.IndexFunc(peers, func(c *pb.Peer) bool { return c.Hostname == MemberList[(idx+1)%n].HostName })
	if idx2 == -1 {
		peers = append(peers, toMemberMsg(idx+1, n))
	}
	idx2 = slices.IndexFunc(peers, func(c *pb.Peer) bool { return c.Hostname == MemberList[(idx+n-1)%n].HostName })
	if idx2 == -1 {
		peers = append(peers, toMemberMsg(idx+n-1, n))
	}
	idx2 = slices.IndexFunc(peers, func(c *pb.Peer) bool { return c.Hostname == MemberList[(idx+n/2)%n].HostName })
	if idx2 == -1 {
		peers = append(peers, toMemberMsg(idx+n/2, n))
	}
	Lock.RUnlock()
	return peers
}

func AddMemberToList(hostname string, timestamp int, status int, incarnation int) {
	Lock.Lock()
	MemberList = append(MemberList, Member{
		HostName:    hostname,
		Timestamp:   timestamp,
		Status:      status,
		Incarnation: incarnation,
		FileInfo:    make(map[string]int),
	})
	Lock.Unlock()
}

// this rpc is called by a peer to request introduction into the p2p system
func (s *server) RequestIntro(ctx context.Context, in *pb.IntroRequest) (*pb.IntroResponse, error) {
	var resp pb.IntroResponse
	Lock.Lock()
	MemberList = append(MemberList, Member{HostName: in.GetHostName(),
		Timestamp:   int(in.GetTimestamp()),
		Status:      2,
		Incarnation: int(in.GetIncarnation()),
		FileInfo:    make(map[string]int)})
	sort.SliceStable(MemberList, func(i, j int) bool {
		return MemberList[i].HostName < MemberList[j].HostName
	})
	Lock.Unlock()
	resp.Peers = introNeighbours(in.GetHostName())
	sort.SliceStable(resp.Peers, func(i, j int) bool {
		return resp.Peers[i].Hostname < resp.Peers[j].Hostname
	})
	return &resp, nil
}

// this function is called to remove a given host from the membership list
func RemoveIndex(hostname string) {
	idx := slices.IndexFunc(MemberList, func(n Member) bool {
		return hostname == n.HostName
	})
	if idx != -1 {
		MemberList = append(MemberList[:idx], MemberList[idx+1:]...)
	}
}

// this function works as a statemachine to transition through the various states
// of a peer in the suspicion mechanism
func waitOnTimerAsync(hostname string, timeoutMarker int) {
	// stopTimer(m)
	go func() {
		Lock.RLock()
		idx := slices.IndexFunc(MemberList, func(m Member) bool {
			return m.HostName == hostname
		})
		if idx == -1 {
			log.Println("idx -1 exit", hostname)
			Lock.RUnlock()
			return
		}
		if MemberList[idx].Timer == nil {
			log.Println("timer nil exit", hostname)
			Lock.RUnlock()
			return
		}
		Lock.RUnlock()
		<-MemberList[idx].Timer.C
		if timeoutMarker == 0 { // Delete Item from list
			// Remove the element at index i from m
			Lock.Lock()
			log.Printf("Deleting Member %v \n", hostname)
			RemoveIndex(hostname)
			Lock.Unlock()
		} else if timeoutMarker == 1 { // Dead -> Cleanup
			Lock.Lock()
			idx := slices.IndexFunc(MemberList, func(m Member) bool {
				return m.HostName == hostname
			})

			if idx == -1 {
				log.Println("idx -1 exit", hostname)
				Lock.Unlock()
				return
			}
			MemberList[idx].Status = 0
			//Check for coordinator failures
			if MemberList[idx].HostName == CoordinatorAddr && MyHostName == StandbyCoordinatorAddr {
				CoordinatorAddr = StandbyCoordinatorAddr
			}
			log.Printf("Starting cleanup timer for %v \n", MemberList[idx].HostName)
			if MemberList[idx].Timer != nil {
				MemberList[idx].Timer.Stop()
			}
			MemberList[idx].Timer = time.NewTimer(3 * time.Second)
			Lock.Unlock()
			waitOnTimerAsync(hostname, 0)
		} else if timeoutMarker == 2 { // Sus -> Dead
			Lock.Lock()
			idx := slices.IndexFunc(MemberList, func(m Member) bool {
				return m.HostName == hostname
			})
			if idx == -1 {
				Lock.Unlock()
				return
			}
			if MemberList[idx].Timer != nil {
				MemberList[idx].Timer.Stop()
			}
			MemberList[idx].Timer = time.NewTimer(time.Millisecond * 1500)
			Lock.Unlock()
			waitOnTimerAsync(hostname, 1)
		}
	}()
}

// function to stop the timer for a Member
func stopTimer(m *Member) {
	m.Timer.Stop()
}

// convert udpmember struct into Member struct
func toMember(u UDPmember) Member {
	t := Member{HostName: u.HostName,
		Timestamp:   u.Timestamp,
		Status:      u.Status,
		Incarnation: u.Incarnation,
		FileInfo:    u.FileInfo,
	}
	return t
}

// convert Member struct into udpmember struct
func toUDPMember(m Member) UDPmember {
	return UDPmember{HostName: m.HostName,
		Timestamp:   m.Timestamp,
		Status:      m.Status,
		Incarnation: m.Incarnation,
		FileInfo:    m.FileInfo,
	}
}

// function to merge peer's membershiplist with incoming membership list
// received using the ping-pong and suspicion mechanism
func mergeMembershipList(inList []UDPmember) {
	var newList []Member
	var newMember Member
	var timerFlag []bool = make([]bool, len(MemberList)+len(inList))
	i := 0
	j := 0

	Lock.Lock()
	for i < len(MemberList) && j < len(inList) {
		if MemberList[i].HostName == inList[j].HostName {
			if MemberList[i].Incarnation < inList[j].Incarnation { //self lower incarnation
				if MemberList[i].Status != 0 { //if i am not dead
					if MemberList[i].Status != inList[j].Status {
						//start timer to death if inStatus = sus and MemberList!=sus or
						if inList[j].Status == 2 && MemberList[i].Timer != nil {
							stopTimer(&MemberList[i])
						}
						MemberList[i].Status = inList[j].Status
						//start remove timer if inStatus = death and MemberList!=death
						if inList[j].Status == 0 || inList[j].Status == 1 {
							Lock.Unlock()
							MemberList[i].Timer = time.NewTimer(0)
							timerFlag[len(newList)] = true
							Lock.Lock()
						}
					}
					MemberList[i].Incarnation = inList[j].Incarnation
				}
			} else { // self >= in incarnation
				if inList[j].Status == 0 && MemberList[i].Status != 0 {
					MemberList[i].Status = inList[j].Status
					//start remove timer
					MemberList[i].Timer = time.NewTimer(0)
					Lock.Unlock()
					timerFlag[len(newList)] = true
					Lock.Lock()
				}
			}
			if MemberList[i].HostName != MyHostName {
				MemberList[i].FileInfo = inList[j].FileInfo
			} else {
				if MemberList[i].FileInfo == nil {
					MemberList[i].FileInfo = map[string]int{}
				}

			}
			newList = append(newList, MemberList[i])
			i += 1
			j += 1
		} else if MemberList[i].HostName < inList[j].HostName {

			newList = append(newList, MemberList[i])
			i += 1
		} else {
			newMember = toMember(inList[j])
			if newMember.Status == 2 {
				newList = append(newList, newMember)
			}
			if inList[j].Status != 2 {
				// start remove timer if inStatus = death
				newMember.Timer = time.NewTimer(0)
				Lock.Unlock()
				timerFlag[len(newList)] = true
				Lock.Lock()
			}
			j += 1
		}
	}
	for i < len(MemberList) {
		newList = append(newList, MemberList[i])
		i += 1
	}
	for j < len(inList) {
		newMember = toMember(inList[j])
		if newMember.Status == 2 {
			newList = append(newList, newMember)
		}
		// start remove timer if inStatus = death
		if newMember.Status == 0 || newMember.Status == 1 {
			newMember.Timer = time.NewTimer(0)
			Lock.Unlock()
			timerFlag[len(newList)] = true
			Lock.Lock()
		}
		j += 1
	}
	for i, ele := range newList {
		if i < len(MemberList) {
			MemberList[i] = ele
		} else {
			MemberList = append(MemberList, ele)
		}
		if timerFlag[i] {
			hostname := MemberList[i].HostName
			status := MemberList[i].Status + 1
			Lock.Unlock()
			waitOnTimerAsync(hostname, status)
			Lock.Lock()
		}

	}
	Lock.Unlock()
}

/*this rpc is called using the test_client to delete previously created known logs in all of the vms
 */
func (s *server) InformDeath(ctx context.Context, in *pb.DeathInfo) (*pb.DeathAck, error) {
	return &pb.DeathAck{}, nil
}

// start the introducer server
func StartIntroducerServer() {
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", TCP_PORT))
	if err != nil {
		log.Printf("failed to listen: %v\n", err)
		panic(err)
	}
	s := grpc.NewServer()
	pb.RegisterIntroInterfaceServer(s, &server{})
	if err := s.Serve(lis); err != nil {
		log.Printf("failed to serve: %v\n", err)
	}
	log.Printf("OUT startIntroducerServer and serving on :%d\n", TCP_PORT)
}

func List_mem() {
	Lock.RLock()
	if len(MemberList) == 0 {
		Lock.RUnlock()
		return
	}
	pretty_print(MemberList)
	Lock.RUnlock()
}

func pretty_print(x interface{}) {
	printer := tableprinter.New().WithBorders(true)
	// Use the custom printer to print the examples:
	if err := printer.Print(x); err != nil {
		panic(err)
	}
}

func List_self() {
	Lock.RLock()
	idx := slices.IndexFunc(MemberList, func(m Member) bool {
		return m.HostName == MyHostName
	})
	if idx != -1 {
		log.Printf("ID of Self %v\n", MemberList[idx])
	} else {
		log.Printf("Not part of any cluster yet!\n")
	}
	pretty_print(MemberList[idx])
	Lock.RUnlock()
}

func Leave() {
	log.Println("Leaving the distributed cluster! :( ")
	leaving = true
}

func StartPeerUDPServer() {
	p := make([]byte, 4096)
	log.Printf("UDP SERVER LISTENING AT %v\n", UDP_PORT_LISTEN)
	udp_addr := net.UDPAddr{
		Port: UDP_PORT_LISTEN,
		IP:   net.ParseIP(MyHostName),
	}
	ser, err := net.ListenUDP("udp", &udp_addr)
	if err != nil {
		log.Printf("Some error %v\n", err)
		return
	}
	for {
		if leaving {
			continue
		}
		var x UDPMsg
		Lock.Lock()
		idx := slices.IndexFunc(MemberList, func(m Member) bool {
			return m.HostName == MyHostName
		})
		if idx == -1 {
			Lock.Unlock()
			continue
		}
		MemberList[idx].Incarnation += 1
		MemberList[idx].Status = 2
		Lock.Unlock()
		var returnList []UDPmember
		Lock.RLock()
		for _, ele := range MemberList {
			returnList = append(returnList, toUDPMember(ele))
		}
		Lock.RUnlock()

		y := UDPMsg{MemberList: returnList}
		out, err := json.Marshal(&y)
		if err != nil {
			panic(err)
		}

		n, remoteaddr, err := ser.ReadFromUDP(p)
		if err := gob.NewDecoder(bytes.NewReader(p[:n])).Decode(&x); err != nil {
			// handle error
			panic(err)

		} else {
			mergeMembershipList(x.MemberList)
		}
		if err != nil {
			log.Printf("Some error  %v\n", err)
			continue
		}
		go sendAckFromServer(ser, remoteaddr, out)

	}
}

func sendAckFromServer(conn *net.UDPConn, udp_addr *net.UDPAddr, response []byte) {
	_, err := conn.WriteToUDP(response, udp_addr)
	if err != nil {
		log.Printf("Couldn't send response %v\n", err)
	}
}

// function to mark Member as suspicious
func markMemberAsSuspicious(neighbour string) {
	Lock.Lock()
	idx := slices.IndexFunc(MemberList, func(m Member) bool {
		return m.HostName == neighbour
	})
	if idx == -1 || MemberList[idx].Status == 1 || MemberList[idx].Status == 0 {
		Lock.Unlock()
		return
	}
	hostname := MemberList[idx].HostName
	//HERE BREAKS
	if MemberList[idx].Status != 1 {
		MemberList[idx].Status = 1
		if MemberList[idx].Timer != nil {
			MemberList[idx].Timer.Stop()
		}
		MemberList[idx].Timer = time.NewTimer(time.Millisecond * 1500)
	}
	Lock.Unlock()
	waitOnTimerAsync(hostname, 2)
}

func FindPeerIdx(peer string) int {
	Lock.RLock()
	idx := slices.IndexFunc(MemberList, func(m Member) bool {
		return m.HostName == peer
	})
	if idx == -1 || MemberList[idx].Status == 0 {
		Lock.RUnlock()
		return -1
	}
	Lock.RUnlock()
	return idx
}

func StartPeerUDPClient() {
	ticker := time.NewTicker(500 * time.Millisecond)
	retChan := make(chan []byte, 10000)
	for _ = range ticker.C {
		if leaving {
			log.Println("Gracefully leaving the process")
			ticker.Stop()
			break
		}
		t := FindNeighbours(MyHostName)
		for i := 0; i < len(t); i++ {
			toAddr := net.UDPAddr{
				Port: UDP_PORT_LISTEN,
				IP:   net.ParseIP(t[i]),
			}
			fromAddr := net.UDPAddr{
				Port: UDP_PORT_DIAL,
				IP:   net.ParseIP(MyHostName),
			}
			conn, err := net.DialUDP("udp", &fromAddr, &toAddr)
			if err != nil {
				log.Println("UDP FAIL", err.Error())
				// retChan <- []byte("Network Partitioned")
				continue
			}
			go sendSynAndWaitForAck(retChan, t[i], conn)
			select {
			case ret := <-retChan:
				if string(ret) == "Network Partitioned" {
					log.Println("No news in 500 milliseconds due to partition.", t[i])
					markMemberAsSuspicious(t[i])
				} else if string(ret) != "CONN CLOSED" {
					//log.Println("Got Ack %s \n", string(ret))
					var response UDPMsg
					ret = bytes.Trim(ret, "\x00")
					err := json.Unmarshal(ret, &response)
					if err != nil {
						log.Println("UDP FAIL", err.Error())
						retChan <- []byte("IDK WHAT ")
						continue
					}
					mergeMembershipList(response.MemberList)
					//log.Printf("%v\n", t)
				}
			case <-time.After(1500 * time.Millisecond):
				log.Println("No news in 500 milliseconds due to timeout.", t[i])
				closeConn(conn, retChan, t[i])
				markMemberAsSuspicious(t[i])
			}
		}
	}
}

func closeConn(conn *net.UDPConn, retChan chan []byte, hostname string) {
	if conn != nil {
		err := conn.Close()
		if err != nil {
		}
	}
}

// function to send a ping and wait for pong
func sendSynAndWaitForAck(retChan chan []byte, hostname string, conn *net.UDPConn) {
	defer closeConn(conn, retChan, hostname)
	//Send Syn
	gob.Register(UDPMsg{})

	Lock.Lock()
	idx := slices.IndexFunc(MemberList, func(m Member) bool {
		return m.HostName == MyHostName
	})
	if idx == -1 {
		Lock.Unlock()
		return
	}
	MemberList[idx].Incarnation += 1
	MemberList[idx].Status = 2
	Lock.Unlock()
	var returnList []UDPmember
	Lock.RLock()
	for _, ele := range MemberList {
		returnList = append(returnList, toUDPMember(ele))
	}
	Lock.RUnlock()

	var x UDPMsg
	x = UDPMsg{MemberList: returnList}
	var buf bytes.Buffer
	if err := gob.NewEncoder(&buf).Encode(x); err != nil {
		panic(err)
	}
	_, err := conn.Write(buf.Bytes())
	if err != nil {
		log.Println("PKK", err.Error(), hostname)
		retChan <- []byte("Network Partitioned")
		return
	}
	//Wait For Ack
	p := make([]byte, 4096)
	_, err = bufio.NewReader(conn).Read(p)
	if err == nil {
	} else {
		log.Println("PKK", err.Error(), hostname)
		retChan <- []byte("Network Partitioned")
		return
	}
	retChan <- p
}

func makeUDPConnection(hostname string) *net.UDPConn {
	toAddr := net.UDPAddr{
		Port: UDP_PORT_LISTEN,
		IP:   net.ParseIP(hostname),
	}
	fromAddr := net.UDPAddr{
		Port: UDP_PORT_DIAL,
		IP:   net.ParseIP(MyHostName),
	}
	conn, err := net.DialUDP("udp", &fromAddr, &toAddr)
	if err != nil {
		log.Printf("Some error %v", err)
		return nil
	}
	return conn
}
